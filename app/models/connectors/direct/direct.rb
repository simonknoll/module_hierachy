module Connectors::Direct::Direct
  extend Connectors::Connector

  def self.special_function
    "Special function for #{self.name}"
  end
end