require 'test_helper'
class ModuleTest < ActiveSupport::TestCase

  test 'base_module' do
    assert Connectors::Connector.connect == "Connecting ..."
  end
  test 'special_module' do
    assert Connectors::Direct::Direct.special_function == "Special function for Connectors::Direct::Direct"
  end
  test 'special_module_inheritance' do
    assert Connectors::Direct::Direct.connect == "Connecting ..."
  end

end